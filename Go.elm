module Go exposing (..)

import Char
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Svg
import Svg.Attributes as SA
import Matrix exposing (..)
import String
import Debug exposing (log)


fieldSize =
    40


nub : List a -> List a
nub x =
    case x of
        [] ->
            []

        x1 :: xs ->
            if List.member x1 xs then
                nub xs
            else
                x1 :: (nub xs)


removeFromList : a -> List a -> List a
removeFromList item list =
    case list of
        [] ->
            []

        x1 :: xs ->
            if x1 == item then
                removeFromList item xs
            else
                x1 :: (removeFromList item xs)


type Color
    = Black
    | White


swapColor : Color -> Color
swapColor x =
    case x of
        Black ->
            White

        White ->
            Black


type Position
    = Stone Color
    | NoStone


type alias Field =
    ( Int, Int )


type alias FieldPosition =
    { color : Color
    , field : Field
    }


type alias Move =
    FieldPosition


type Msg
    = TryMove Move


type alias GameConfiguration =
    { boardSize : Int
    , torus : Bool
    }


type alias Game =
    List Move


type alias Model =
    { game : Game
    , board : Board
    , errorMessage : Maybe ErrorMessage
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TryMove move ->
            case (playMove model.board move) of
                Ok board ->
                    let
                        game =
                            model.game ++ [ move ]
                    in
                        ( { model | game = game, board = board, errorMessage = Nothing }, Cmd.none )

                Err errorMessage ->
                    ( { model | errorMessage = Just errorMessage }, Cmd.none )


type alias Group =
    { color : Color
    , stones : List Field
    , liberties : List Field
    }


type alias Board =
    { stones : Matrix Position
    , size : Int
    , torus : Bool
    , nextMove : Color
    , groups : List Group
    , captives :
        { white : Int
        , black : Int
        }
    }


type alias ErrorMessage =
    String


anyMember : List a -> List a -> Bool
anyMember x y =
    case x of
        [] ->
            False

        x1 :: xs ->
            List.member x1 y || anyMember xs y


{-| Return list of neighbor fields
-}
neighborFields : Board -> Field -> List Field
neighborFields board ( x, y ) =
    let
        rawNeigborFieldNumbers =
            [ ( x - 1, y ), ( x + 1, y ), ( x, y - 1 ), ( x, y + 1 ) ]
    in
        if board.torus then
            List.map (\( x, y ) -> ( x % board.size, y % board.size )) rawNeigborFieldNumbers
        else
            List.filter (\( x, y ) -> x >= 0 && x < board.size && y >= 0 && y < board.size) rawNeigborFieldNumbers


{-| Return tuple (own, others, rest) of neighbor groups of given field
-}
neighborGroups : Board -> List Group -> Field -> Color -> ( List Group, List Group, List Group )
neighborGroups board groups field color =
    let
        allNeighborGroups =
            List.filter (\g -> anyMember g.stones (neighborFields board field)) groups
    in
        ( List.filter (\g -> color == g.color) allNeighborGroups
        , List.filter (\g -> color /= g.color) allNeighborGroups
        , List.filter (\g -> not <| List.member g allNeighborGroups) groups
        )


{-| Return tuple (own, rest) of neighbor groups of given field
-}
getOwnNeighborGroups : Board -> List Group -> Field -> Color -> ( List Group, List Group )
getOwnNeighborGroups board groups field color =
    let
        allNeighborGroups =
            List.filter (\g -> anyMember g.stones (neighborFields board field)) groups

        ownNeighborGroups =
            List.filter (\g -> color == g.color) allNeighborGroups
    in
        ( ownNeighborGroups
        , List.filter (\g -> not <| List.member g ownNeighborGroups) groups
        )


{-| Return tuple (own, rest) of neighbor groups of given field
-}
getOppositeNeighborGroups : Board -> List Group -> Field -> Color -> ( List Group, List Group )
getOppositeNeighborGroups board groups field color =
    getOwnNeighborGroups board groups field (swapColor color)


isNeighbor : Board -> Field -> Field -> Bool
isNeighbor board f1 f2 =
    List.member f1 (neighborFields board f2)


{-| Return number of neighbors between two groups
-}
neighborCount : Board -> List Field -> List Field -> Int
neighborCount board x y =
    case x of
        [] ->
            0

        x1 :: xs ->
            (if List.any (isNeighbor board x1) y then
                1
             else
                0
            )
                + neighborCount board xs y


neighborsInGroup : Board -> List Field -> List Field -> List Field
neighborsInGroup board x y =
    case y of
        [] ->
            []

        y1 :: ys ->
            if List.any (isNeighbor board y1) x then
                y1 :: neighborsInGroup board x ys
            else
                neighborsInGroup board x ys


playMove : Board -> Move -> Result ErrorMessage Board
playMove board move =
    if (move.color) /= (board.nextMove) then
        (Err "Wrong color")
    else
        let
            ( col, row ) =
                move.field
        in
            case (Matrix.get col row board.stones) of
                Nothing ->
                    Err ("Field " ++ toString move.field ++ "does not exist")

                Just field ->
                    case field of
                        NoStone ->
                            let
                                -- place stone
                                stones_ =
                                    Matrix.set col row (Stone move.color) board.stones

                                ( oppositeNeighborGroups, otherGroups ) =
                                    getOppositeNeighborGroups board board.groups move.field move.color

                                -- update / kill opponent group
                                ( oppositeNeighborGroups_, otherGroups_, stones__, captives_ ) =
                                    let
                                        removeStones : Group -> Matrix Position -> Matrix Position
                                        removeStones killedGroup stones =
                                            List.foldr (\( col, row ) stones -> Matrix.set col row NoStone stones) stones killedGroup.stones

                                        updateOtherGroups : Group -> List Group -> List Group
                                        updateOtherGroups killedGroup otherGroups =
                                            List.map (\g -> { g | liberties = g.liberties ++ neighborsInGroup board g.stones killedGroup.stones }) otherGroups

                                        update group ( gsAcc, otherGroups, stones, captives ) =
                                            if List.length group.liberties == 1 then
                                                -- kill
                                                ( gsAcc
                                                , updateOtherGroups group otherGroups
                                                , removeStones group stones
                                                , if move.color == Black then
                                                    { captives | white = captives.white + List.length group.stones }
                                                  else
                                                    { captives | black = captives.black + List.length group.stones }
                                                )
                                            else
                                                ( { group | liberties = removeFromList move.field group.liberties } :: gsAcc, otherGroups, stones, captives )
                                    in
                                        List.foldr update ( [], otherGroups, stones_, board.captives ) oppositeNeighborGroups

                                ( ownNeighborGroups, otherGroups__ ) =
                                    getOwnNeighborGroups board otherGroups_ move.field move.color

                                ownGroup =
                                    let
                                        fieldIsEmpty : Field -> Matrix Position -> Bool
                                        fieldIsEmpty ( col, row ) matrix =
                                            case (Matrix.get col row matrix) of
                                                Just NoStone ->
                                                    True

                                                _ ->
                                                    False

                                        currentFieldLiberties =
                                            List.filter (\f -> fieldIsEmpty f stones__) (neighborFields board move.field)

                                        currentGroupLiberties =
                                            removeFromList move.field <| nub <| List.foldr (++) currentFieldLiberties (List.map .liberties ownNeighborGroups)
                                    in
                                        if List.length currentGroupLiberties == 0 then
                                            -- suicide
                                            Nothing
                                        else
                                            Just { color = move.color, stones = List.foldr (++) [ move.field ] (List.map .stones ownNeighborGroups), liberties = currentGroupLiberties }
                            in
                                case ownGroup of
                                    Nothing ->
                                        Err "Cannot commit suicide"

                                    Just ownGroup ->
                                        Ok
                                            { board
                                                | stones = stones__
                                                , nextMove = swapColor board.nextMove
                                                , groups = [ ownGroup ] ++ oppositeNeighborGroups_ ++ otherGroups__
                                                , captives = captives_
                                            }

                        Stone color ->
                            Err "Field already occupied"


view : Model -> Html Msg
view model =
    let
        fieldName col row =
            String.fromList [ (Char.fromCode (Char.toCode 'A' + col)) ] ++ toString (row + 1)

        viewMove move =
            let
                ( col, row ) =
                    move.field
            in
                [ viewStoneSvg move.color, text <| fieldName col row ]

        nextMove =
            model.board.nextMove

        svgContext contents displayBlock =
            Svg.svg
                [ style <|
                    if displayBlock then
                        [ ( "display", "block" ) ]
                    else
                        []
                , SA.width <| toString fieldSize
                , SA.height <| toString fieldSize
                ]
                contents

        viewStone color =
            [ Svg.circle
                [ SA.cx <| toString <| fieldSize // 2
                , SA.cy <| toString <| fieldSize // 2
                , SA.r <| toString <| fieldSize // 2 - 2
                , SA.stroke "black"
                , SA.strokeWidth "2"
                , SA.fill
                    (if color == White then
                        "white"
                     else
                        "black"
                    )
                ]
                []
            ]

        viewStoneSvg color =
            svgContext (viewStone color) False

        viewField colIdx rowIdx =
            case (Matrix.get colIdx rowIdx model.board.stones) of
                Nothing ->
                    -- todo: break!
                    div [] []

                Just field ->
                    let
                        showField field =
                            let
                                left =
                                    if model.board.torus then
                                        False
                                    else
                                        colIdx == 0

                                right =
                                    if model.board.torus then
                                        False
                                    else
                                        colIdx == (model.board.size - 1)

                                top =
                                    if model.board.torus then
                                        False
                                    else
                                        rowIdx == 0

                                bottom =
                                    if model.board.torus then
                                        False
                                    else
                                        rowIdx == (model.board.size - 1)

                                grid =
                                    [ Svg.line
                                        [ SA.x1 <|
                                            toString
                                                (if left then
                                                    fieldSize // 2
                                                 else
                                                    0
                                                )
                                        , SA.x2 <|
                                            toString
                                                (if right then
                                                    fieldSize // 2
                                                 else
                                                    fieldSize
                                                )
                                        , SA.y1 <| toString <| fieldSize // 2
                                        , SA.y2 <| toString <| fieldSize // 2
                                        , SA.stroke "black"
                                        , SA.strokeWidth "1"
                                        ]
                                        []
                                    , Svg.line
                                        [ SA.x1 <| toString <| fieldSize // 2
                                        , SA.x2 <| toString <| fieldSize // 2
                                        , SA.y1 <|
                                            toString
                                                (if top then
                                                    fieldSize // 2
                                                 else
                                                    0
                                                )
                                        , SA.y2 <|
                                            toString
                                                (if bottom then
                                                    fieldSize // 2
                                                 else
                                                    fieldSize
                                                )
                                        , SA.stroke "black"
                                        , SA.strokeWidth "1"
                                        ]
                                        []
                                    ]

                                star =
                                    if List.member colIdx [ 3, model.board.size // 2, model.board.size - 1 - 3 ] && List.member rowIdx [ 3, model.board.size // 2, model.board.size - 1 - 3 ] then
                                        [ Svg.circle [ SA.cx <| toString <| fieldSize // 2, SA.cy <| toString <| fieldSize // 2, SA.r "3", SA.stroke "black", SA.strokeWidth "1", SA.fill "black" ] [] ]
                                    else
                                        []

                                stone =
                                    case field of
                                        NoStone ->
                                            []

                                        Stone color ->
                                            viewStone color
                            in
                                [ svgContext (grid ++ star ++ stone) True ]

                        fieldStyle field =
                            case field of
                                NoStone ->
                                    [ ( "cursor", "cell" ) ]

                                _ ->
                                    []
                    in
                        td [ style <| [ ( "padding", "0" ) ] ++ fieldStyle field, onClick <| TryMove { color = nextMove, field = ( colIdx, rowIdx ) } ] <|
                            -- showText field
                            showField field

        viewRow rowIdx =
            tr [] <| List.map (\i -> viewField i rowIdx) (List.range 0 (model.board.size - 1))

        showErrorMessage errorMessage =
            case errorMessage of
                Just errorMessage ->
                    div [] [ text errorMessage ]

                Nothing ->
                    div [] []
    in
        div []
            [ table [ style [ ( "border-collapse", "collapse" ) ] ] <| List.map viewRow (List.range 0 (model.board.size - 1))
            , div [] [ text <| "Captives:", div [] <| List.repeat model.board.captives.white <| viewStoneSvg White, div [] <| List.repeat model.board.captives.black <| viewStoneSvg Black ]
            , div [] [ text <| "Next: ", viewStoneSvg nextMove ]
            , showErrorMessage model.errorMessage
            , div [] <| List.map (\m -> span [] (viewMove m)) model.game

            --, div [] [ text <| toString model.board.groups ]
            ]


init : GameConfiguration -> ( Model, Cmd Msg )
init config =
    let
        board : Board
        board =
            { stones = Matrix.repeat config.boardSize config.boardSize NoStone
            , size = config.boardSize
            , torus = config.torus
            , nextMove = Black
            , groups = []
            , captives = { black = 0, white = 0 }
            }
    in
        ( { board = board
          , game = []
          , errorMessage = Nothing
          }
        , Cmd.none
        )


main : Program Never Model Msg
main =
    Html.program
        { init = init { boardSize = 19, torus = True }
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
